package com.example.server.controller;

import com.example.server.entity.Person;
import com.example.server.service.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/person")
public class PersonController {
    PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/byId/{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable("id") Long id){
        Person person = personService.getPersonById(id);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Person>> getAllPersons(){
        List<Person> personList = personService.getAllPersons();
        return new ResponseEntity<>(personList, HttpStatus.OK);
    }
}
