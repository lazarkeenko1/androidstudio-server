package com.example.server.repositroy;

import com.example.server.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    @Query("select p from Person p where p.id = ?1")
    Person findPersonById(Long id);

    @Query("select p from Person p")
    List<Person> getAllPersons();
}
