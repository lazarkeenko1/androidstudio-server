package com.example.server.service;

import com.example.server.entity.Person;
import com.example.server.repositroy.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {
    PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person getPersonById(Long id){
        return personRepository.findPersonById(id);
    }

    public List<Person> getAllPersons(){
        return personRepository.getAllPersons();
    }
 }
